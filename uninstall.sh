#!/bin/bash

HERE=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)

pushd "${HERE}" || exit 1
    # shellcheck disable=SC1091
    source ./includes

    echo
    echo -n "Started UNinstalling ${app_name} on "
    date
    echo

    # allow user to abort
    sleep 10

    ./stop.sh

    sudo systemctl disable weather.trend.day.timer
    sudo systemctl disable weather.trend.month.timer
    sudo systemctl disable weather.update.timer

    sudo systemctl disable weather.again.service

popd || exit

echo
echo "*********************************************************"
echo -n "Finished UNinstallation of ${app_name} on "
date
echo
