#!/bin/bash

# Use stop.sh to stop all daemons in one go
# You can use update.sh to get everything started again.

HERE=$(cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)

pushd "${HERE}" || exit 1
    # shellcheck disable=SC1091
    source ./includes

    sudo systemctl stop weather.trend.day.timer &
    sudo systemctl stop weather.trend.month.timer &
    sudo systemctl stop weather.update.timer &

    wait; sudo systemctl stop weather.again.service

popd || exit

echo
echo "To re-start all daemons, use:"
echo "./update.sh"
