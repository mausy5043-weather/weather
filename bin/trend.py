#!/usr/bin/env python3

"""Create trendbargraphs for various periods of weatherdata."""

import argparse
from datetime import datetime as dt

import matplotlib.pyplot as plt

import againlib as alib

DATABASE = "/srv/array1/datastore/sqlite3/weatherdata.sqlite3"


def fetch_last_day(hours_to_fetch):
    """Gather data for the requested number of hours

    Args:
        hours_to_fetch (int): number of hours worth of data to retrieve

    Returns:
        (list, list, list, list): data labels,
                                  temperature,
                                  pressure,
                                  humidity data

    """
    config = alib.add_time_line({'grouping': '%m-%d %Hh',
                                 'period': hours_to_fetch,
                                 'timeframe': 'hour',
                                 'database': DATABASE,
                                 'table': 'weather'
                                 })
    temperature, data_lbls = alib.get_historic_data(config, parameter='temperature')
    pressure, data_lbls = alib.get_historic_data(config, parameter='pressure')
    humidity, data_lbls = alib.get_historic_data(config, parameter='humidity')
    return data_lbls, temperature, pressure, humidity


def fetch_last_month(days_to_fetch):
    """Gather data for the requested number of days

    Args:
        days_to_fetch (int): number of days worth of data to retrieve

    Returns:
        (list, list, list, list): data labels,
                                  temperature,
                                  pressure,
                                  humidity data

    """
    config = alib.add_time_line({'grouping': '%m-%d',
                                 'period': days_to_fetch,
                                 'timeframe': 'day',
                                 'database': DATABASE,
                                 'table': 'weather'
                                 })
    temperature, data_lbls = alib.get_historic_data(config, parameter='temperature')
    pressure, data_lbls = alib.get_historic_data(config, parameter='pressure')
    humidity, data_lbls = alib.get_historic_data(config, parameter='humidity')
    # solrad not included in data
    # solrad, data_lbls = alib.get_historic_data(config, parameter='solrad', somma=True)
    # solrad = solrad / 1000
    return data_lbls, temperature, pressure, humidity  # , solrad


def plot_graph(output_file, data_tuple, plot_title):
    """Create graphs of the data-sets

    Args:
        output_file (str) : path/filename of the graph to create
        data_tuple (tuple) : data structure to turn into a graph
        plot_title (str) : title to put above the plot

    Returns:
          Creates a graph to output_file

    Todo:
        use pandas
    """
    data_lbls = data_tuple[0]
    temperature = data_tuple[1][:, 0]
    temperature_min = data_tuple[1][:, 1]
    temperature_max = data_tuple[1][:, 2]
    pressure = data_tuple[2][:, 0]
    pressure_min = data_tuple[2][:, 1]
    pressure_max = data_tuple[2][:, 2]
    humidity = data_tuple[3][:, 0]
    humidity_min = data_tuple[3][:, 1]
    humidity_max = data_tuple[3][:, 2]

    moist = alib.moisture(temperature, humidity, pressure) * 1000
    """
    --- Start debugging:
    np.set_printoptions(precision=3)
    print("data_lbls   : ", np.size(data_lbls), data_lbls[-5:])
    print(" ")
    print("temperatuur : ", np.size(temperature), temperature[-5:])
    print(" ")
    print("vochtigheid : ", np.size(humidity), humidity[-5:])
    print("luchtdruk   : ", np.size(pressure), pressure[-5:])
    print("vochtgehalte: ", np.size(moist), moist[-5:])
    # print("zon.straling: ", np.size(solrad), solrad[-5:])
    print(" ")
    --- End debugging.
    """
    # Set the bar width
    bar_width = 0.75
    # positions of the left bar-boundaries
    tick_pos = list(range(1, len(data_lbls) + 1))

    # *** TEMPERATURE / HUMIDITY
    plt.rc('font', size=6.5)
    dummy, ax1 = plt.subplots(1, figsize=(10, 2.5))

    """
    # ###############################
    # Create a line plot of temperature/humidity
    # ###############################
    """

    # humidity
    ahpla = 0.4
    ax1.errorbar(tick_pos, humidity,
                 yerr=[humidity - humidity_min, humidity_max - humidity],
                 elinewidth=10,
                 label='Vochtigheid',
                 alpha=ahpla,
                 color='blue',
                 marker='o'
                 )
    ax1.set_ylabel("[%]")
    ax1.legend(loc='upper left', framealpha=0.2)
    y_lo = min(humidity) - 5
    y_hi = max(humidity) + 5
    if y_lo > 20:
        y_lo = 20
    if y_hi < 100:
        y_hi = 100
    ax1.set_ylim([y_lo, y_hi])

    # Set general plotting stuff
    ax1.set_xlabel("Datetime")
    ax1.grid(which='major',
             axis='y',
             color='k',
             linestyle='--',
             linewidth=0.5
             )
    plt.xticks(tick_pos,
               data_lbls,
               rotation=-60
               )

    ax2 = ax1.twinx()

    # temperature
    ahpla = 0.7
    ax2.errorbar(tick_pos, temperature,
                 yerr=[temperature - temperature_min, temperature_max - temperature],
                 elinewidth=5,
                 label='Temperatuur',
                 alpha=ahpla,
                 color='red',
                 marker='o'
                 )
    ax2.set_ylabel("[degC]")
    ax2.legend(loc='upper right', framealpha=0.2)
    y_lo = min(temperature) - 5
    y_hi = max(temperature) + 5
    if y_lo > -10:
        y_lo = -10
    if y_hi < 30:
        y_hi = 30
    ax2.set_ylim([y_lo, y_hi])

    # Fit every nicely
    plt.title(f'{plot_title}')
    plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
    # plt.tight_layout()
    plt.savefig(fname=f'{output_file}TH.png', format='png')

    # *** PRESSURE
    plt.rc('font', size=6.5)
    dummy, ax3 = plt.subplots(1, figsize=(10, 2.5))
    """
    # ###############################
    # Create a bar plot of pressure
    # ###############################
    """
    ax3.bar(tick_pos, pressure_min,
            width=bar_width,
            label='Pressure (min)',
            alpha=ahpla,
            color='lightgreen',
            align='center',
            bottom=0
            )
    ax3.bar(tick_pos, pressure_max - pressure_min,
            width=bar_width,
            label='Pressure (max)',
            alpha=ahpla,
            color='green',
            align='center',
            bottom=pressure_min
            )

    # Set Axes stuff
    ax3.set_ylabel("[mbara]")
    ax3.set_xlabel("Datetime")
    ax3.grid(which='major',
             axis='y',
             color='k',
             linestyle='--',
             linewidth=0.5
             )
    ax3.legend(loc='upper left', framealpha=0.2)

    # Set plot stuff
    plt.xticks(tick_pos, data_lbls, rotation=-60)
    # plt.title(f'{plot_title}')

    # Fit every nicely
    plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
    y_lo = min(pressure) - 10
    y_hi = max(pressure) + 10
    if y_lo > 990:
        y_lo = 990
    if y_hi < 1030:
        y_hi = 1030
    ax3.set_ylim([y_lo, y_hi])
    # plt.tight_layout()
    plt.savefig(fname=f'{output_file}P.png', format='png')

    # *** MOISTURE
    ahpla = 0.4
    plt.rc('font', size=6.5)
    dummy, ax6 = plt.subplots(1, figsize=(10, 2.5))
    """
    # ###############################
    # Create a bar plot of moisture
    # ###############################
    """
    ax6.bar(tick_pos, moist,
            width=bar_width,
            label='Moisture',
            alpha=ahpla,
            color='blue',
            align='center',
            bottom=0
            )

    # Set Axes stuff
    ax6.set_ylabel("[g water/m3 lucht]")
    ax6.set_xlabel("Datetime")
    ax6.grid(which='major',
             axis='y',
             color='k',
             linestyle='--',
             linewidth=0.5
             )
    ax6.legend(loc='upper left', framealpha=0.2)

    # Set plot stuff
    plt.xticks(tick_pos, data_lbls, rotation=-60)
    # plt.title(f'{plot_title}')

    # Fit everything nicely
    plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
    y_lo = min(moist) - 0.2
    y_hi = max(moist) + 0.2
    if y_lo > 4.0:
        y_lo = 4.0
    if y_hi < 10.0:
        y_hi = 10.0
    ax6.set_ylim([y_lo, y_hi])
    # plt.tight_layout()
    plt.savefig(fname=f'{output_file}M.png', format='png')


def main():
    """
      This is the main loop
      """
    global OPTION

    if OPTION.hours:
        plot_graph('/tmp/weather/site/img/again_pastday_',
                   fetch_last_day(OPTION.hours),
                   f"Trend afgelopen dagen ({dt.now().strftime('%d-%m-%Y %H:%M:%S')})"
                   )
    if OPTION.days:
        plot_graph('/tmp/weather/site/img/again_pastmonth_',
                   fetch_last_month(OPTION.days),
                   f"Trend afgelopen maand ({dt.now().strftime('%d-%m-%Y %H:%M:%S')})"
                   )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create a trendgraph")
    parser.add_argument('-hr',
                        '--hours',
                        type=int,
                        help='create an hour-trend of <HOURS>'
                        )
    parser.add_argument('-d',
                        '--days',
                        type=int,
                        help='create a day-trend of <DAYS>'
                        )
    OPTION = parser.parse_args()
    if OPTION.hours == 0:
        OPTION.hours = 50
    if OPTION.days == 0:
        OPTION.days = 50
    main()
