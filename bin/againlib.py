#!/usr/bin/env python3

"""Common functions for use by again*.py scripts.

Todo:
    * Use argparse to replace get_cli_param()
"""

import datetime as dt
import sqlite3 as s3
import sys

import numpy as np


def add_time_line(config):
    """Create a list of UX-epochs spanning the configured interval ending at the current date/time.

    Args:
        config (dict): timeline configuration

    Returns: modified version of config

    """
    final_epoch = int(dt.datetime.now().timestamp())
    step_epoch = 10 * 60
    multi = 3600
    if config['timeframe'] == 'hour':
        multi = 3600
    if config['timeframe'] == 'day':
        multi = 3600 * 24
    if config['timeframe'] == 'month':
        multi = 3600 * 24 * 31
    if config['timeframe'] == 'year':
        multi = 3600 * 24 * 366
    start_epoch = int((final_epoch
                       - (multi * config['period']))
                      / step_epoch) \
                  * step_epoch
    config['timeline'] = np.arange(start_epoch,
                                   final_epoch,
                                   step_epoch,
                                   dtype='int'
                                   )
    return config


def get_cli_params(expected_amount):
    """Retrieve the CLI argument

    Todo:
        * replace by use of argparse

    Args:
        expected_amount (int): number of arguments to be expected

    Returns:
        str: CLI-parameters

    """
    if len(sys.argv) != (expected_amount + 1):
        print(f"{expected_amount} arguments expected, {len(sys.argv) - 1} received.")
        sys.exit(0)
    return sys.argv[1]


def get_historic_data(dicti, parameter=None, from_start_of_year=False, include_today=True, somma=False):
    """Fetch historic data from SQLITE3 database.

    Args:
        dicti (dict): configuration settings to use
        parameter (str): columnname to be collected
        from_start_of_year (bool): fetch data from start of year or not (default: False)
        include_today (bool): also fetch today's (incomplete) data (default: True)
        somma (bool): return sum of grouped data (True) OR return avg of grouped data (default: False)

    Returns:
        list of data (float) and list of labels (str)
    """
    period = dicti['period']
    if from_start_of_year:
        interval = f"datetime(datetime(\'now\', \'-{period + 1} {dicti['timeframe']}\'), \'start of year\')"
    else:
        interval = f"datetime(\'now\', \'-{period + 1} {dicti['timeframe']}\')"
    if include_today:
        and_where_not_today = ''
    else:
        and_where_not_today = 'AND (sample_time <= datetime(\'now\', \'-1 day\'))'
    db_con = s3.connect(dicti['database'])
    with db_con:
        db_cur = db_con.cursor()
        db_cur.execute(f"SELECT sample_epoch, {parameter}"
                       f" FROM {dicti['table']}"
                       f" WHERE (sample_time >= {interval}) {and_where_not_today}"
                       f" AND stn_id = 1"
                       f" ORDER BY sample_epoch ASC"
                       f";"
                       )
        db_data = db_cur.fetchall()

    data = np.array(db_data)
    for i, row in enumerate(data):
        for c in row:
            if c is None:
                data = np.delete(data, i, 0)

    # interpolate the data to monotonic 10minute intervals provided by dicti['timeline']
    ret_epoch, ret_intdata = interplate(dicti['timeline'],
                                        np.array(data[:, 0], dtype=int),
                                        np.array(data[:, 1], dtype=int)
                                        )

    # group the data by dicti['grouping']
    ret_lbls, ret_grpdata = fast_group_data(ret_epoch, ret_intdata, dicti['grouping'], somma)

    ret_data = ret_grpdata
    return ret_data[-period:], ret_lbls[-period:]


def interplate(epochrng, epoch, data):
    """Interpolate any missing datapoints to fit a neat
    monotonic dataset with 10 minute intervals

    Args:
        epochrng (list): requested monotonic set of epochs
        epoch (list): actual epochs
        data (list): actual data in a NumPy array

    Returns:
        list of epochs and the interpolated data as a Numpy array
    """
    datarng = np.interp(epochrng, epoch, data)
    return epochrng, datarng


def group_data(x_epochs, y_data, grouping, somma):
    """DEPRECATED

    Args:
        x_epochs:
        y_data:
        grouping:
        somma:

    Returns:

    """
    x_texts = np.array([dt.datetime.fromtimestamp(i).strftime(grouping) for i in x_epochs],
                       dtype='str')
    unique_x_texts = np.array(sorted(set(x_texts)), dtype='str')
    y_shape = (np.shape(unique_x_texts)[0], 3)
    returned_y_data = np.zeros(y_shape)

    for idx, ut in enumerate(unique_x_texts):
        indices = np.where(x_texts == ut)[0]
        y_min = y_max = None
        if len(indices) > 1:
            if somma:
                y_pnt = np.sum(y_data[indices[0]:indices[-1]])
            else:
                y_pnt = np.mean(y_data[indices[0]:indices[-1]])
                y_min = np.min(y_data[indices[0]:indices[-1]])
                y_max = np.max(y_data[indices[0]:indices[-1]])
        else:
            y_pnt = y_data[-1]
        returned_y_data[idx] = [y_pnt, y_min, y_max]

    return unique_x_texts, returned_y_data


def fast_group_data(x_epochs, y_data, grouping, somma):
    """A faster version of group_data().

    Args:
        x_epochs (list): x-data
        y_data (list): y-data
        grouping (str): strftime format string
        somma (bool): if True return summed data per interval

    Returns:
        NumPy arrays containing labeltexts and data
    """
    # convert y-values to numpy array
    y_data = np.array(y_data)
    # convert epochs to text
    x_texts = np.array([dt.datetime.fromtimestamp(i).strftime(grouping) for i in x_epochs],
                       dtype='str')
    """x_texts = ['12-31 20h' '12-31 21h' '12-31 21h' '12-31 21h' '12-31 21h' '12-31 21h'
                 '12-31 21h' '12-31 22h' '12-31 22h' '12-31 22h' '12-31 22h' '12-31 22h'
                 :
                 '01-01 09h' '01-01 10h' '01-01 10h' '01-01 10h' '01-01 10h' '01-01 10h'
                 '01-01 10h']
    """
    # compress x_texts to a unique list
    # order must be preserved
    _, loc1 = np.unique(x_texts, return_index=True)
    loc1 = np.sort(loc1)
    unique_x_texts: str = x_texts[loc1]
    # preform the returned y-data array
    y_shape = (np.shape(unique_x_texts)[0], 3)
    returned_y_data = np.zeros(y_shape)

    loc2 = len(x_texts) - 1 - np.unique(np.flip(x_texts), return_index=True)[1]
    loc2 = np.sort(loc2)

    for idx in range(0, len(loc1)):
        y_pnt = None
        y_min = None
        y_max = None
        if loc1[idx] == loc2[idx]:
            data_y = y_data[loc1[idx]]
        else:
            data_y = y_data[loc1[idx]:loc2[idx]]
        if somma:
            y_pnt = np.sum(data_y)
        else:
            y_pnt = np.mean(data_y)
            y_min = np.min(data_y)
            y_max = np.max(data_y)
        returned_y_data[idx] = [y_pnt, y_min, y_max]

    return unique_x_texts, returned_y_data


def moisture(temperature, relative_humidity, pressure):
    """Calculate the moisture-content of the air.

    Args:
        temperature (float) : temperature in degC
        relative_humidity (float) : relative humidity in %
        pressure (float) : pressure in mbara

    Returns:
        moisture-content (g/m2)
    """
    kelvin = temperature + 273.15
    pascal = pressure * 100
    rho = (287.04 * kelvin) / pascal

    es = 611.2 * np.exp(17.67 * (kelvin - 273.15) / (kelvin - 29.65))
    rvs = 0.622 * es / (pascal - es)
    rv = relative_humidity / 100. * rvs
    qv = rv / (1 + rv)
    moistair = qv * rho  # kg water per m3 air
    return np.array(moistair)
