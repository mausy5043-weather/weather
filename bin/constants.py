#!/usr/bin/env python3

API_UPDATE_INTERVAL = 600  # seconds

AGAIN = {'reporttime': 600,
         'cycles': 1,
         'samplespercycle': 1,
         'lockfile': "/tmp/weather/21.lock",
         'table': "weather",
         'schema': "",
         'insert': "INSERT "
                   "INTO weather "
                   "(sample_time, sample_epoch, "
                   "stn_id, temperature, humidity, pressure, solrad) "
                   "VALUES(?, ?, ?, ?, ?, ?, ?)",
         'databasefile': "/srv/array1/datastore/sqlite3/weatherdata.sqlite3"
         }
