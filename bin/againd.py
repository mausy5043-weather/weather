#!/usr/bin/env python3

"""Communicate with the weerlive.nl website to fetch weather data from selected stations

- temperature
- humidity
- pressure
- solar radiation

Store the data from the website of in an sqlite3 database.
"""

import datetime as dt
import os
import sqlite3
import sys
import syslog
import time
import traceback

import mausy5043funcs.fileops3 as mf
import mausy5043libs.libsignals3 as ml
import mausy5043libs.libsqlite3 as msql

import constants
import weerlivelib as wllib

# constants
IS_JOURNALD = os.path.isfile('/bin/journalctl')
DEBUG = False
HERE = os.path.realpath(__file__).split('/')
# runlist id :
MYID = HERE[-1]
# app_name :
MYAPP = HERE[-3]
MYROOT = "/".join(HERE[0:-3])
# host_name :
NODE = os.uname()[1]

weerlive = wllib.Weerlive()

T_MEMORY = [None] * len(weerlive.STATIONS)
H_MEMORY = [None] * len(weerlive.STATIONS)
P_MEMORY = [None] * len(weerlive.STATIONS)
S_MEMORY = [None] * len(weerlive.STATIONS)


def main():
    """Execute main loop.
    """
    killer = ml.GracefulKiller()
    report_time = constants.API_UPDATE_INTERVAL
    weer_db = msql.SqlDatabase(database=constants.AGAIN['databasefile'],
                               schema=constants.AGAIN['schema'],
                               table=constants.AGAIN['table'],
                               insert=constants.AGAIN['sqlcmd']
                               )
    sample_time = report_time / constants.AGAIN['samplespercycle']

    weer_db.test_db_connection()
    pause_time = time.time()
    while not killer.kill_now:
        if time.time() > pause_time:
            start_time = time.time()
            if not DEBUG:
                time.sleep(60)  # wait for server to update the data
            do_work(weer_db)

            pause_time = (sample_time
                          - (time.time() - start_time)
                          - (start_time % sample_time)
                          + time.time()
                          )
            if pause_time > 0:
                mf.syslog_trace(f"Waiting  : {pause_time - time.time():.1f}s", False, DEBUG)
                mf.syslog_trace("................................", False, DEBUG)
                # time.sleep(pause_time)
            else:
                mf.syslog_trace(f"Behind   : {pause_time - time.time():.1f}s", False, DEBUG)
                mf.syslog_trace("................................", False, DEBUG)
        else:
            time.sleep(1.0)


def do_work(db):
    """Process the data from the website

    Returns:
        none
    """
    global T_MEMORY
    global H_MEMORY
    global P_MEMORY
    global S_MEMORY
    dt_format = '%m/%d/%Y %H:%M:%S'
    new_dt_format = '%Y-%m-%d %H:%M:%S'
    arr_temperature = T_MEMORY
    arr_humidity = H_MEMORY
    arr_pressure = P_MEMORY
    arr_solrad = S_MEMORY
    date_time = ''
    epoch = 0

    for stn_idx, stn in enumerate(weerlive.STATIONS):
        stn_id = int(stn['id'])
        stn_name = stn['name']
        station_data = weerlive.query_website(stn_name)
        if DEBUG:
            print(f"Station name : {stn_name}")
        # recall previous values
        temperature = arr_temperature[stn_idx]
        humidity = arr_humidity[stn_idx]
        pressure = arr_pressure[stn_idx]
        solrad = arr_solrad[stn_idx]
        for key in station_data:
            if key == 'temp':
                temperature = station_data[key].strip()
                if temperature == '-':
                    temperature = None
            if key == 'lv':
                humidity = station_data[key].strip()
                if humidity == '-':
                    humidity = None
            if key == 'luchtd':
                pressure = station_data[key].strip()
                if pressure == '-':
                    pressure = None
            if key == 'zonintensiteitWM2':
                solrad = station_data[key].strip()
                if solrad == '-':
                    # None is used to denote an invalid or unknown datapoint.
                    solrad = None
            if key == 'datum':
                epoch = int(dt.datetime.strptime(station_data[key].strip(), dt_format).timestamp())
                date_time = time.strftime(new_dt_format, time.localtime(epoch))

        if epoch == 0:
            epoch = int(dt.datetime.now().timestamp())
            epoch = epoch - (epoch % constants.API_UPDATE_INTERVAL)
            date_time = time.strftime(new_dt_format, time.localtime(epoch))
        # store new values
        arr_temperature[stn_idx] = temperature
        arr_humidity[stn_idx] = humidity
        arr_pressure[stn_idx] = pressure
        arr_solrad[stn_idx] = solrad
        # push to DB
        datablock = {'sample_time': date_time,
                     'smple_epoch': epoch,
                     'stn_id': stn_id,
                     'temperature': temperature,
                     'humidity': humidity,
                     'pressure': pressure,
                     'solrad': solrad}
        if DEBUG:
            print(f"Station data : {datablock}")
        db.queue([datablock])

    datablock = [date_time,
                 epoch,
                 1,
                 weerlive.guesstimate_local_value(arr_temperature),
                 weerlive.guesstimate_local_value(arr_humidity),
                 weerlive.guesstimate_local_value(arr_pressure),
                 None  # requires payed subscription f"{guesstimate_local_value(arr_solrad):.1f}"
                 ]
    if DEBUG:
        mf.syslog_trace(f"Local data : {datablock}")
    db.queue([datablock])

    T_MEMORY = arr_temperature
    H_MEMORY = arr_humidity
    P_MEMORY = arr_pressure
    S_MEMORY = arr_solrad


def do_add_to_database(results, fdatabase, sql_cmd):
    """Add data to the database.

    Args:
        results (list): list of data to be inserted
        fdatabase (str): path/filename of the database file
        sql_cmd (str): SQLite3 command used to insert data into the database

    Returns:
        none
    """
    # Get the time and date in human-readable form and UN*X-epoch...
    # conn = None
    # cursor = None
    # we don't record the datetime of addition to the database here.
    # instead we use the datetime we got from buienradar.
    # So, we can just dump the data into sqlite3.
    mf.syslog_trace(f"   @: {results[0]}", False, DEBUG)
    mf.syslog_trace(f"    : {results[1:]}", False, DEBUG)

    err_flag = True
    cursor = None
    conn = None
    while err_flag:
        try:
            conn = create_db_connection(fdatabase)
            cursor = conn.cursor()
            if not epoch_is_present_in_database(cursor, results[1], results[2]):
                mf.syslog_trace(f">>> : {results[0]} = {results[3]} {results[4]} {results[5]} {results[6]}", False,
                                DEBUG)
                cursor.execute(sql_cmd, results)
                # TODO: use context manager to handle closing iso exception handler
                cursor.close()
                conn.commit()
                conn.close()
            else:
                mf.syslog_trace(f"Skip: {results[0]}", False, DEBUG)
            err_flag = False
        except sqlite3.OperationalError:
            if cursor:
                cursor.close()
            if conn:
                conn.close()


def epoch_is_present_in_database(db_cur, epoch, stn_id):
    """Test if result (UX-epoch) is already present in the database

    Args:
        db_cur (Cursor): sqlite3 database cursor object to be used
        epoch (int): UX-epoch to be checked
        stn_id (int): the station ID (1=local)

    Returns:
        (bool): True if data is present in the database
                for the given site at or after the given UX-epoch.
    """

    db_cur.execute(f"SELECT MAX(sample_epoch) FROM weather WHERE stn_id = {stn_id};")
    db_epoch = db_cur.fetchone()[0]
    if db_epoch:
        if db_epoch >= epoch:
            return True
    return False


if __name__ == "__main__":
    # initialise logging
    syslog.openlog(ident=f'{MYAPP}.{MYID.split(".")[0]}', facility=syslog.LOG_LOCAL0)

    if len(sys.argv) == 2:
        if sys.argv[1] == 'start':
            main()
        elif sys.argv[1] == 'restart':
            main()
        elif sys.argv[1] == 'debug':
            # assist with debugging.
            DEBUG = True
            mf.syslog_trace("Debug-mode started.", syslog.LOG_DEBUG, DEBUG)
            print("Use <Ctrl>+C to stop.")
            main()
        else:
            print("Unknown command")
            sys.exit(2)
    else:
        print("usage: {0!s} start|restart|debug".format(sys.argv[0]))
        sys.exit(2)
    print("And it's goodnight from him")
    sys.exit(0)
