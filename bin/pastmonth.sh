#!/bin/bash

# query hourly totals for a period of two days and trend them

HERE=$(cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)

pushd "${HERE}" >/dev/null || exit 1
    # shellcheck disable=SC1091
    source ../includes
    ./trend.py --days 0
    ./upload.sh --upload
popd >/dev/null || exit

CURRENT_EPOCH=$(date +'%s')
# Keep upto 180 days of data from external sites
PURGE_EPOCH=$(echo "${CURRENT_EPOCH} - (180 * 24 * 3600)" | bc)
sqlite3 "${database_filename}" \
        "DELETE FROM weather WHERE sample_epoch < ${PURGE_EPOCH} AND stn_id > 1;"

# Keep upto 6 years of local data (stn_id=1)
PURGE_EPOCH=$(echo "${CURRENT_EPOCH} - (6 * 366 * 24 * 3600)" | bc)
sqlite3 "${database_filename}" \
        "DELETE FROM weather WHERE sample_epoch < ${PURGE_EPOCH} AND stn_id = 1;"
