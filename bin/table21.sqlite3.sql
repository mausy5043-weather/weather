# SQLite3 script
# create table for storage of weather station data

DROP TABLE IF EXISTS weather;

CREATE TABLE weather (
  sample_time   datetime NOT NULL,
  sample_epoch  integer NOT NULL,
  stn_id        integer,
  temperature   real,
  humidity      real,
  pressure      real,
  solrad        real
  );

CREATE INDEX idx_time ON weather(sample_time);
CREATE INDEX idx_epoch ON weather(sample_epoch);
CREATE INDEX idx_stn ON weather(stn_id);
