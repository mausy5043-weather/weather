#!/usr/bin/env python3

import collections
import configparser
import json
import syslog
import time
import urllib.request
from urllib import error as urlerr

import mausy5043funcs.fileops3 as mf  # noqa

DEBUG = False


class Weerlive:
    def __init__(self):
        self.STATIONS = [{'id': 6340, 'distance': 14.0, 'name': 'Woensdrecht'},
                         {'id': 6350, 'distance': 1.5, 'name': 'Rijen'},
                         {'id': 6356, 'distance': 9.0, 'name': 'Gorichem'},
                         {'id': 6370, 'distance': 9.5, 'name': 'Eindhoven'},
                         {'id': 6351, 'distance': 0.5, 'name': 'Tilburg'}
                         ]
        self.URL = "http://weerlive.nl/api/json-data-10min.php?key="
        self.inikey = configparser.ConfigParser()
        self.inikey.read('/srv/array1/datastore/sqlite3/.weerlive.key')
        self.API_KEYS = collections.deque([self.inikey.get('KEYS', 'key_a'),
                                           self.inikey.get('KEYS', 'key_b'),
                                           self.inikey.get('KEYS', 'key_c')
                                           ])
        self.data = dict()

    def guesstimate_local_value(self, data_source):
        """Guess the local value of a parameter based on the known values for various
        stations and given the distance to each station.

        Args:
            data_source (list): list of datapoints

        Returns:
            (float): the estimated (guessed) local value

        """
        station_inverse = list()
        data_array = list()
        # '-100.0'(legacy) or None is used to denote an invalid or unknown datapoint.
        # remove '-100.0' and None values from the data_array and the same index from station_inverse
        for idx, x in enumerate(data_source):
            if x not in ['-100.0', None]:
                station_inverse.append(1 / self.STATIONS[idx]['distance'])
                data_array.append(float(x))

        if data_array:
            # determine normalisation factor for each remaining station
            station_weight = [x / sum(station_inverse) for x in station_inverse]
            # determine normalised value
            normalised_data = [float(int(station_weight[i] * x * 10)) / 10
                               for i, x in enumerate(data_array)
                               ]
            # guess the local value
            guessed_value = round(sum(normalised_data), 1)
            mf.syslog_trace(f"Guess : {guessed_value}", False, DEBUG)
        else:
            guessed_value = None
        return guessed_value

    def query_website(self, station_name):
        """Query the API of liveweer.nl for data from a given weatherstation.

        Args:
            station_name (str): name of the station for which to query the API

        Returns:
            (dict): data returned

        """
        location = '&locatie='
        retries = 3  # number of retries before giving up
        retry_pause = 11  # seconds to wait before retrying
        stn_data = {}

        while True:
            try:
                self.API_KEYS.rotate(1)
                key = self.API_KEYS[0]
                api_call = "".join([self.URL, key, location, station_name])
                response = urllib.request.urlopen(api_call)
                data = json.loads(response.read())
                # remove useless outer shell
                stn_data = data['liveweer'][0]
                mf.syslog_trace(f"<<< : {stn_data}", False, DEBUG)
            except (urlerr.URLError, json.JSONDecodeError):
                retries -= 1
                if retries:
                    mf.syslog_trace("Wait while retrying...", False, DEBUG)
                    time.sleep(retry_pause)
                    continue
                else:
                    mf.syslog_trace("WEERLIVE not responding!", syslog.LOG_WARNING, DEBUG)
                    stn_data = {}
                    break
            break
        return stn_data
