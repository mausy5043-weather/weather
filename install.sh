#!/bin/bash

# this repo gets installed either manually by the user or automatically by
# a `*boot` repo.

ME=$(whoami)
required_commonlibversion="1.0.2"
commonlibbranch="v1_0"

if [ "${ME}" == "root" ]; then
    sudo=""
else
    sudo="sudo "
fi

echo -n "Started installing WEATHER on "
date
minit=$(echo ${RANDOM}/555 | bc)
echo "MINIT = ${minit}"

install_package() {
    # See if packages are installed and install them.
    package=${1}
    echo "*********************************************************"
    echo "* Requesting ${package}"
    status=$(pacman -Qe "${package}" 2>/dev/null | wc -l)
    if [ "${status}" -eq 0 ]; then
        echo "* Installing ${package}"
        echo "*********************************************************"
        ${sudo} pacman -Syu --noconfirm "${package}"
    else
        echo "* Already installed !!!"
        echo "*********************************************************"
    fi
}

${sudo} pacman -Syu
install_package "inetutils"
install_package "lftp"
install_package "procps-ng"

# Python 3 package and associates
install_package "python"
install_package "python-pip"

# SQLite3 support
install_package "sqlite3"

echo
echo "*********************************************************"
python3 -m pip install --upgrade pip setuptools wheel
pushd "${HERE}" || exit 1
  python3 -m pip install -r requirements.txt
popd || exit 1

commonlibversion=$(python3 -m pip freeze | grep mausy5043 | cut -c 26-)
if [ "${commonlibversion}" != "${required_commonlibversion}" ]; then
    echo
    echo "*********************************************************"
    echo "Install common python functions..."
    python3 -m pip uninstall -y mausy5043-common-python
    python3 -m pip install "git+https://gitlab.com/mausy5043-installer/mausy5043-common-python.git@${commonlibbranch}#egg=mausy5043-common-python"
    echo
    echo -n "Installed: "
    python3 -m pip list | grep mausy5043
    echo
fi

pushd "${HOME}/weather" || exit 1
    # To suppress git detecting changes by chmod:
    git config core.fileMode false
    # set the branch
    if [ ! -e "${HOME}/.weather.branch" ]; then
        echo "master" >"${HOME}/.weather.branch"
    fi
    chmod -x ./services/*

    # Recover the database from the server
    # ./bin/bakrecdb.sh --install

    # install services and timers
    sudo cp ./services/*.service /etc/systemd/system/
    sudo cp ./services/*.timer /etc/systemd/system/
    #
    sudo systemctl daemon-reload
    #
    sudo systemctl enable weather.trend.day.timer
    sudo systemctl enable weather.trend.month.timer
    sudo systemctl enable weather.update.timer

    sudo systemctl enable weather.again.service
    #
    sudo systemctl start weather.trend.day.timer
    sudo systemctl start weather.trend.month.timer
    sudo systemctl start weather.update.timer # this will also start the daemon!

    sudo systemctl start weather.again.service

popd || exit 1

echo
echo "*********************************************************"
echo -n "Finished installation of WEATHER on "
date
